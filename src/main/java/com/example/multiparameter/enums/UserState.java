package com.example.multiparameter.enums;

public enum UserState {
    NONE("NONE"),
    PENDING("PENDING"),
    ACTIVE("ACTIVE"),
    DEACTIVATED("DEACTIVATED");

    private final String value;

    UserState(String value){
        this.value = value;
    }

    @Override
    public String toString(){
        return value;
    }
}
