package com.example.multiparameter.services;

import com.example.multiparameter.entities.User;
import com.example.multiparameter.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    private final UserRepository userRepository;
    @Autowired
    public UserService(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    public List<User> findUsersByQueryParams(Specification<User> spec){
        return userRepository.findAll(spec);
    }
}
