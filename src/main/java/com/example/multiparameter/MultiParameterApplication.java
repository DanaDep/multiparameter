package com.example.multiparameter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MultiParameterApplication {

	public static void main(String[] args) {
		SpringApplication.run(MultiParameterApplication.class, args);
	}

}
