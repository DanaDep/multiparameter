create table public.users (
                              user_id uuid primary key,
                              first_name character varying,
                              last_name character varying,
                              email character varying,
                              state text,
                              is_admin boolean,
                              created_on timestamp without time zone,
                              updated_on timestamp without time zone
);