insert into public.users (user_id, first_name, last_name, email, state, is_admin, created_on, updated_on)
values  ('0f17cc75-9e5a-43a1-a87e-399f37a66a1e', 'Jane', 'Doe', 'jane.doe@mail.com', 'ACTIVE', true, '2023-07-21 12:36:32.622000', '2023-07-22 12:36:32.622000'),
        ('1f17cc75-9e5a-43a1-a87e-399f37a66a1e', 'John', 'Doe', 'john.doe@mail.com', 'ACTIVE', true, '2023-07-20 12:36:32.622000', '2023-07-22 12:36:32.622000'),
        ('2f17cc75-9e5a-43a1-a87e-399f37a66a1e', 'Sam', 'Smith', 'sam.smith@mail.com', 'PENDING', false, '2023-07-19 12:36:32.622000', '2023-07-22 12:36:32.622000'),
        ('3f17cc75-9e5a-43a1-a87e-399f37a66a1e', 'Andrew', 'Newman', 'andrew.newman@mail.com', 'DEACTIVATED', true, '2023-07-25 12:36:32.622000', '2023-07-22 12:36:32.622000'),
        ('4f17cc75-9e5a-43a1-a87e-399f37a66a1e', 'Monica', 'Geller', 'monica.geller@mail.com', 'NONE', false, '2023-07-23 12:36:32.622000', '2023-07-22 12:36:32.622000');