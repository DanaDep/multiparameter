# Getting Started

### MultiParameter App
This is a simple demo app that exemplifies how to use JPA Specifications.

It provides a simple REST GET resource which is able to provide different results based on different query parameters.